from pythonforandroid.recipe import PythonRecipe

class TrytondCompanyRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/company/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond']
    python_depends = []
    site_packages_name = 'trytond-company'
    call_hostpython_via_targetpython = False

recipe = TrytondCompanyRecipe()
