from pythonforandroid.recipe import PythonRecipe

class TrytondPartyRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/party/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond', 'trytond-country']
    python_depends = []
    site_packages_name = 'trytond-party'
    call_hostpython_via_targetpython = False

recipe = TrytondPartyRecipe()
