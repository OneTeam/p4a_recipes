from pythonforandroid.recipe import PythonRecipe


class TrytondRecipe(PythonRecipe):

    version = '5.4'
    url = 'http://hg.tryton.org/trytond/archive/{version}.tar.gz'
    depends = [('python3'), 'setuptools', 'sqlite3', 'lxml', 'relatorio']
    python_depends = ['werkzeug', 'python-sql', 'python-dateutil', 'wrapt',
                      'polib', 'python-stdnum', 'passlib']
    site_packages_name = 'trytond'
    call_hostpython_via_targetpython = False
    patches = ['trytond.patch']


recipe = TrytondRecipe()
