from pythonforandroid.recipe import PythonRecipe

class PythonMagicRecipe(PythonRecipe):
    version = '0.4.15'
    url = 'https://files.pythonhosted.org/packages/source/p/python-magic/python-magic-{version}.tar.gz'
    depends = [('python3'), 'setuptools', 'libmagic']
    python_depends = []
    site_packages_name = 'python-magic'
    call_hostpython_via_targetpython = False

recipe = PythonMagicRecipe()
