from pythonforandroid.recipe import PythonRecipe

class TrytondCountryRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/country/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond']
    python_depends = []
    site_packages_name = 'trytond-country'
    call_hostpython_via_targetpython = False

recipe = TrytondCountryRecipe()
