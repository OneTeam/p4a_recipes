from pythonforandroid.recipe import PythonRecipe

class TrytondCurrencyRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/currency/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond', 'trytond-company']
    python_depends = []
    site_packages_name = 'trytond-currency'
    call_hostpython_via_targetpython = False

recipe = TrytondCurrencyRecipe()
