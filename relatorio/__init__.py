from pythonforandroid.recipe import PythonRecipe

class RelatorioRecipe(PythonRecipe):
    version = '0.9.1'
    url = 'http://hg.tryton.org/relatorio/archive/{version}.tar.gz'
    depends = [('python3'), 'setuptools', 'puremagic', 'lxml']
    python_depends = ['Genshi']
    site_packages_name = 'relatorio'
    call_hostpython_via_targetpython = False
    patches = ['relatorio.patch']

recipe = RelatorioRecipe()
