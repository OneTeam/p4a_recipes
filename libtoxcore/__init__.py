from pythonforandroid.recipe import Recipe
from pythonforandroid.util import current_directory
from pythonforandroid.logger import shprint, info
from os.path import join, exists
import sh

class LibtoxcoreRecipe(Recipe):
    version = '0.2.10'

    url = 'https://github.com/TokTok/c-toxcore/archive/v{version}.tar.gz'
    depends = ['libsodium']
    built_libraries = {'libtoxcore.so': 'build/.libs'}


    def get_recipe_env(self, arch, with_flags_in_cc=True):
        env = super(LibtoxcoreRecipe, self).get_recipe_env(arch, with_flags_in_cc)
        libsodium_recipe = self.get_recipe('libsodium', self.ctx)
        libsodium_dir =  libsodium_recipe.get_build_dir(arch.arch)
        env['LIBSODIUM_CFLAGS'] = ' -I' + join(libsodium_dir, 'src', 'libsodium', 'include')
        env['LIBSODIUM_LIBS'] = ' -L' + join(libsodium_dir, 'src', 'libsodium', '.libs')
        return env

    def build_arch(self, arch):
        env = self.get_recipe_env(arch)

        libsodium_recipe = self.get_recipe('libsodium', self.ctx)
        libsodium_dir =  libsodium_recipe.get_build_dir(arch.arch)

        with current_directory(self.get_build_dir(arch.arch)):
            if not exists('configure'):
                shprint(sh.bash, "autogen.sh")
            configure = sh.Command('./configure')

            config_args = {
                '--host={}'.format(arch.command_prefix),
                '--disable-soname-versions',
                '--disable-rt',
                '--disable-av',
                '--disable-static',
                '--disable-tests',
                '--disable-testing',
                '--enable-shared',
            }
            shprint(configure, *config_args, _env=env)
            shprint(sh.make, _env=env)

recipe = LibtoxcoreRecipe()
