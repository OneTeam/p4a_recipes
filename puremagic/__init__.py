from pythonforandroid.recipe import PythonRecipe

class PuremagicRecipe(PythonRecipe):
    version = '1.6'
    url = 'https://files.pythonhosted.org/packages/source/p/puremagic/puremagic-{version}.tar.gz'
    depends = [('python3'), 'setuptools']
    python_depends = []
    conflicts = ['magic']
    site_packages_name = 'puremagic'
    call_hostpython_via_targetpython = False

recipe = PuremagicRecipe()
