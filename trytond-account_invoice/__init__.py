from pythonforandroid.recipe import PythonRecipe

class TrytondAccount_invoiceRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/account_invoice/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond', 'trytond-account', 'trytond-account_product', 'trytond-company', 'trytond-currency', 'trytond-product', 'trytond-party']
    python_depends = ['simpleeval']
    site_packages_name = 'trytond-account_invoice'
    call_hostpython_via_targetpython = False

recipe = TrytondAccount_invoiceRecipe()
