from pythonforandroid.recipe import PythonRecipe

class TrytondProduct_attributeRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/product_attribute/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond']
    python_depends = []
    site_packages_name = 'trytond-product_attribute'
    call_hostpython_via_targetpython = False

recipe = TrytondProduct_attributeRecipe()
