from pythonforandroid.recipe import PythonRecipe
from os.path import join

class PytoxRecipe(PythonRecipe):
    version = 'master'
    url = 'https://github.com/TokTok/py-toxcore-c/archive/{version}.zip'
    depends = [('python3'), 'libtoxcore', 'setuptools']
    site_packages_name = 'pytox'
    patches = ['pytox.patch']
    call_hostpython_via_targetpython = False
    
    def get_recipe_env(self, arch):
        env = super(PytoxRecipe, self).get_recipe_env(arch)
        libtoxcore_recipe = self.get_recipe('libtoxcore', self.ctx)
        libtoxcore_dir = libtoxcore_recipe.get_build_dir(arch.arch)

        env['CFLAGS'] += ' -I' + join(libtoxcore_dir, 'toxcore')
        env['LDFLAGS'] += ' -L' + join(libtoxcore_dir, 'build', '.libs')
        return env

    
recipe = PytoxRecipe()
