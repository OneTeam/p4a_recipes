from pythonforandroid.recipe import PythonRecipe

class TrytondProductRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/product/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond', 'trytond-company']
    python_depends = []
    site_packages_name = 'trytond-product'
    call_hostpython_via_targetpython = False

recipe = TrytondProductRecipe()
