from pythonforandroid.recipe import PythonRecipe

class TrytondAccount_productRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/account_product/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond', 'trytond-account', 'trytond-product']
    python_depends = []
    site_packages_name = 'trytond-account_product'
    call_hostpython_via_targetpython = False

recipe = TrytondAccount_productRecipe()
