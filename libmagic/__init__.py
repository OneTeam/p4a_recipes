from pythonforandroid.recipe import Recipe
from pythonforandroid.util import current_directory
from pythonforandroid.logger import shprint
from multiprocessing import cpu_count
import sh

class LibmagicRecipe(Recipe):
    # TODO require same version host file command
    version = '5.35'
    url = 'http://ftp.astron.com/pub/file/file-{version}.tar.gz'
    patches = ['libmagic.patch']
    built_libraries = { 'libmagic.so': 'src/.libs'}
    

    def build_arch(self, arch):
        with current_directory(self.get_build_dir(arch.arch)):
            env = self.get_recipe_env(arch)
            configure = sh.Command('./configure')
            build_arch = 'x86-unknown-linux-gnu'
            shprint(configure,
                    '--build=' + build_arch,
                    '--host='+ arch.command_prefix,
                    '--prefix='+ self.get_build_dir(arch.arch),
                    '--disable-soname-versions',
                    '--enable-shared',
                    _env=env)
            shprint(sh.make, '-j', str(cpu_count()), _env=env)


recipe = LibmagicRecipe()
