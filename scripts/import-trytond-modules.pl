use strict;
use warnings;

use File::Spec;
use File::Basename;
use Cwd qw(getcwd abs_path);

if (getcwd eq dirname(abs_path($0))) {
  print "not allowed created modules inside scripts, please run in directory you want recipes\n";
  exit -1;
}

sub render_recipe {
  my ($module, $version, $python_depends, @depends) = @_;
  
  my $class_suffix = ucfirst $module;

  my @suffix_depends = map {"'trytond-$_'"} @depends;
  unshift @suffix_depends, "'trytond'";
  
  my $out_depends = join ', ', @suffix_depends ;

  my @suffix_python_depends = map {"'$_'"} @{$python_depends};
  my $out_python_depends = join ', ', @suffix_python_depends;
  return <<END
from pythonforandroid.recipe import PythonRecipe

class Trytond${class_suffix}Recipe(PythonRecipe):
    version = '${version}'
    url = 'http://hg.tryton.org/modules/$module/archive/{version}.tar.gz'
    depends = [('python3'), ${out_depends}]
    python_depends = [${out_python_depends}]
    site_packages_name = 'trytond-${module}'
    call_hostpython_via_targetpython = False

recipe = Trytond${class_suffix}Recipe()
END
}

# TODO construir arbol de dependencia a partir de tryton.cfg
# y genererar lista de descarga
my @modules = (
               ['country', '5.4', [], qw()],
               ['party', '5.4', [], qw(country)],
               ['company', '5.4', [], qw()],
               ['currency', '5.4', [], qw(company)],
               ['product', '5.4', [], qw(company)],
               ['product_attribute', '5.4', [], qw()],
               ['product_classification', '5.4', [], qw( product)],
               ['account', '5.4', [], qw(party company currency)],
               ['account_product', '5.4', [], qw(account product)],
               ['account_invoice', '5.4', ['simpleeval'], qw(account account_product company currency product party)],
              );

for my $spec (@modules) {
  my ($module, $version, $python_depends, @depends) = @{$spec};

  my $dirname = File::Spec->catfile(getcwd, "trytond-${module}");
  my $out = render_recipe($module, $version, $python_depends, @depends);
  
  mkdir $dirname;
  open(my $fh, '>', File::Spec->catfile($dirname, "__init__.py"))
    or die"can't open $!";
  print $fh $out;
  close $fh;
  
  print "new recipe: $dirname\n";
}
