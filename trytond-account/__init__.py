from pythonforandroid.recipe import PythonRecipe

class TrytondAccountRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/account/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond', 'trytond-party', 'trytond-company', 'trytond-currency']
    python_depends = []
    site_packages_name = 'trytond-account'
    call_hostpython_via_targetpython = False

recipe = TrytondAccountRecipe()
