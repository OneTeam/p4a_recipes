from pythonforandroid.recipe import PythonRecipe

class TrytondProduct_classificationRecipe(PythonRecipe):
    version = '5.4'
    url = 'http://hg.tryton.org/modules/product_classification/archive/{version}.tar.gz'
    depends = [('python3'), 'trytond', 'trytond-product']
    python_depends = []
    site_packages_name = 'trytond-product_classification'
    call_hostpython_via_targetpython = False

recipe = TrytondProduct_classificationRecipe()
